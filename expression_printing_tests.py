import unittest

from expressions import Constant, Negation


class ExpressionPrintingTestCase(unittest.TestCase):
    def test_print_constant_should_be_constant_string(self):
        constant = Constant(7)
        self.assertEqual(constant.__str__(), "7")

    def test_print_negation_should_be_negated_constant_string(self):
        negation = Negation(Constant(7))
        self.assertEqual(negation.__str__(), "-7")

    def test_print_negation_negation_should_be_constant_string(self):
        negation_negation = Constant(7).negated().negated()
        self.assertEqual(negation_negation.__str__(), "7")


if __name__ == '__main__':
    unittest.main()
