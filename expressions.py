class Expression:
    def negated(self):
        return Negation(self)

class Constant(Expression):
    def __init__(self, value):
        self.value = value

    def evaluate(self, variable_values=dict()):
        return self.value

    def __str__(self):
        return str(self.value)

class Variable(Expression):
    def __init__(self, name):
        self.name = name

    def evaluate(self, variable_values=dict()):
        return variable_values[self.name]


class Negation(Expression):
    def __init__(self, expression):
        self.expression = expression

    def evaluate(self, variable_values=dict()):
        return - self.expression.evaluate(variable_values)

    def negated(self):
        return self.expression

    def __str__(self):
        return "-" + self.expression.__str__()

class BinaryExpression(Expression):
    def __init__(self, left_expression, right_expression):
        self.leftExpression = left_expression
        self.rightExpression = right_expression

    def evaluate(self, variable_values=dict()):
        left_value = self.leftExpression.evaluate(variable_values)
        right_value = self.rightExpression.evaluate(variable_values)
        return self.do_evaluate(left_value, right_value)

class Addition(BinaryExpression):

    def do_evaluate(self, left_value, right_value):
        return left_value + right_value


class Multiplication(BinaryExpression):

    def do_evaluate(self, left_value, right_value):
        return left_value * right_value