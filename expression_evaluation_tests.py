import unittest

from expressions import Constant, Negation, Addition, Multiplication, Variable


class ExpressionEvaluationTestCase(unittest.TestCase):
    def test_constant_value_should_evaluate_to_value(self):
        constant = Constant(7)
        self.assertEqual(constant.evaluate(), 7)

    def test_negate_constant_should_evaluate_to_negated_value(self):
        negated_constant = Negation(Constant(7))
        self.assertEqual(negated_constant.evaluate(), -7)

    def test_negate_negation_should_evaluate_to_value(self):
        negated_negation = Negation(Negation(Constant(7)))
        self.assertEqual(negated_negation.evaluate(), 7)

    def test_adding_constants_should_evaluate_to_sum_of_values(self):
        sum = Addition(Constant(-3), Constant(7))
        self.assertEqual(sum.evaluate(), 4)

    def test_adding_complex_expressions_should_evaluate_to_sum_of_values(self):
        addition = Addition(Negation(Constant(-3)), Negation(Constant(7)))
        self.assertEqual(addition.evaluate(), -4)

    def test_multiplying_complex_expressions_should_evaluate_to_multiplication_of_values(self):
        multiplication = Multiplication(Negation(Constant(-3)), Negation(Constant(7)))
        self.assertEqual(multiplication.evaluate(), -21)

    def test_constant_negated_should_negate_constant(self):
        self.assertEqual(Constant(7).negated().evaluate(), -7)

    def test_negation_negated_should_be_constant(self):
        self.assertEqual(Constant(7).negated().negated().evaluate(), 7)

    def test_addition_negated_should_negate_addition(self):
        self.assertEqual(Addition(Constant(7), Constant(3))
                         .negated().evaluate(), -10)

    def test_variable_should_evaluate_to_variable_value(self):
        variable = Variable("x")
        increment = Addition(variable, Constant(1))
        variable_values = { "x" : 3 }
        self.assertEqual(increment.evaluate(variable_values), 4)


if __name__ == '__main__':
    unittest.main()
